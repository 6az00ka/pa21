<?php
include_once 'header.php';
$id = $_REQUEST['id'];
?>
    <form method="POST">
        <div class="input-group">
            <input type="text" class="form-control" name="svalue" placeholder="Введите ключевое слово"
                   aria-describedby="basic-addon1">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
            <div class="input-group-append">
                <button class="btn btn-dark" type="submit">Искать</button>
            </div>
        </div>
    </form>
    <br><br>
<?php
$sv = $_POST['svalue'];
$filename = "/tmp/upload-${id}.csv";
$check = 0;
if (isset($sv)) {
    $handle1 = fopen($filename, "r");
    echo '<h2>Полученные данные:</h2>';
    echo '<br>';
    while (($data = fgetcsv($handle1, 1000, ";")) !== FALSE) {
        foreach ($data as $v) {
            if (strpos($v, $sv) !== false) {
                if ($check == 0) {
                    $check = 1;
                    echo '<table class="table table-dark block1">';
                    echo '<tbody>';
                }
                echo '<tr class="d-flex">';
                for ($i = 0; $i < count($data); $i++) {
                    echo '<td class="col">' . $data[$i] . '</td>';
                }
                echo '</tr>';
            }
        }
    }
    if ($check == 1) {
        echo '</tbody>';
        echo '</table>';
    } else {
        echo '<div class="alert alert-danger" role="alert">Ничего не найдено</div>';
    }
    fclose($filename);
    $check1=0;
    $handle2=fopen($filename, "r");
    while (($data = fgetcsv($handle2, 1000, ";")) !== FALSE) {
        if ($check1 == 0) {
            $check1 = 1;
            echo '<table class="table table-dark block2">';
            echo '<tbody>';
        }
        echo '<tr class="d-flex">';
        for ($i = 0; $i < count($data); $i++) {
            echo '<td class="col">' . $data[$i] . '</td>';
        }
        echo '</tr>';
    }
    if ($check1 == 1) {
        echo '</tbody>';
        echo '</table>';
    }
    fclose($filename);
    echo '<div class="custom-control custom-switch">
        <input type="checkbox" class="custom-control-input" id="sw1">
        <label class="custom-control-label" for="sw1">Снять фильтр</label>
        </div>
        <script>
            var checkbox = document.querySelector(\'input[type="checkbox"]\');
            checkbox.addEventListener(\'change\', function () {
                if (checkbox.checked) {
                    document.getElementsByClassName(\'block2\')[0].style= "display: block";
                    document.getElementsByClassName(\'block1\')[0].style= "display: none";
                } else {
                    document.getElementsByClassName(\'block2\')[0].style= "display: none";
                    document.getElementsByClassName(\'block1\')[0].style= "display: block";
                }   
            });
        </script>';
} else {
    $handle = fopen($filename, "r");
    echo '<h2>Текущие данные:</h2>';
    echo '<br>';
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        if ($check == 0) {
            $check = 1;
            echo '<table class="table table-dark">';
            echo '<tbody>';
        }
        echo '<tr class="d-flex">';
        for ($i = 0; $i < count($data); $i++) {
            echo '<td class="col">' . $data[$i] . '</td>';
        }
        echo '</tr>';
    }
    if ($check == 1) {
        echo '</tbody>';
        echo '</table>';
    }
    else{
        echo '<div class="alert alert-danger" role="alert">Нет данных!</div>';
    }
    fclose($filename);
}
?>
<?php include_once 'footer.php'; ?>