<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CSV Datawrapper</title>
    <style>
        html { height: 100%; }
        body {
            min-height:100%;
            position:relative;
            padding-bottom: 50px;

        }
        .footer {
            position: absolute;
            left: 0;
            right: 0;
            bottom: 0;
            height: 60px;
        }

        .container:after {
            content: '';
            display: block;
            margin-bottom: 50px;
        }

        .mainlink {
            color: white;
        }

        .mainlink:hover {
            color: green;
        }

        .mainlink:visited {
            color: white;
        }

        .myta{
            resize: none;
        }
        .block2{
            display: none;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg py-3 navbar-dark bg-dark shadow-sm">
    <div class="container">
        <a href="http://pa21.std-842.ist.mospolytech.ru/" class="navbar-brand">
            <img src="https://mospolytech.ru/storage/b53b3a3d6ab90ce0268229151c9bde11/images/Logo2.jpg" width="150" alt="" class="d-inline-block align-middle mr-2">
            <span class="text-uppercase font-weight-bold">CSV Datawrapper</span>
        </a>
        <div id="navbarSupportedContent" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="http://pa21.std-842.ist.mospolytech.ru/" class="nav-link">Главная страница<span class="sr-only">(current)</span></a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<br>
<div class="container">