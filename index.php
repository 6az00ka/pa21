<?php include_once 'header.php'; ?>
    <div class="jumbotron">
        <h1 class="display-3">Создайте свою таблицу</h1>
        <hr class="my-2">
        <p>На данном сайте можно загрузить или ввести вручную данные в формате CSV.</p>
        <p class="lead">
            <a class="btn btn-outline-dark btn-lg" href="upload.php" role="button">Начать</a>
        </p>
    </div>
    <div class="card-group">
        <div class="card">
            <img class="card-img-top" src="1.png">
            <div class="card-body">
                <h4>Загрузка вручную</h4>
                <p class="card-text">Скопируйте данные CSV-БД.</p>
            </div>
        </div>
        <div class="card">
            <img class="card-img-top" src="2.png">
            <div class="card-body">
                <h4>Загрузка из файла</h4>
                <p class="card-text">Выберите файл .csv на компьютере.</p>
            </div>
        </div>
        <div class="card">
            <img class="card-img-top" src="3.png">
            <div class="card-body">
                <h4>Таблица</h4>
                <p class="card-text">Вывод таблицы с данными.</p>
            </div>
        </div>
    </div>
<?php include_once 'footer.php'; ?>