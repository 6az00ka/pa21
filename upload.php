<?php include_once 'header.php'; ?>
    <div class="sidebar"><h3>Выберите способ загрузки данных</h3></div>
    <br><br>
    <div class="row">
            <div class="col-5">
                <h4>1) Ввод вручную</h4>
                <p>Скопируйте данные в текстовое поле справа</p>
                <form action="datacode.php" method="post">
                    <div class="control-group">
                        <textarea class="form-control myta z-depth-1" rows="10" id="up" name="upload" placeholder="Введите данные"></textarea>
                    </div>
                    <br><br>
                    <button type="submit" class="btn btn-dark">Отправить текст</button>
                </form>
            </div>
            <div class="col-5">
                <h4>2) Загрузка из файла</h4>
                <p>Выберите файл на компьютере</p>
                <form action="datacode.php" enctype="multipart/form-data" method="post">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="f" id="customFile">
                        <label class="custom-file-label" for="customFile"></label>
                    </div>
                    <br><br>
                    <button type="submit" class="btn btn-dark">Отправить файл</button>
                </form>
            </div>
        </div>
<?php include_once 'footer.php'; ?>